import { PipeTransform, Pipe } from '@angular/core';

declare var _: any;

@Pipe({name: 'toKeyValuePair'})
export class ToKeyValuePairPipe implements PipeTransform {
    transform(value, args:string[]) : any {
        let keys = value;
        let result = [];

        for (let key in keys) {
            let obj = _.find(result, function(obj) { return obj.key == value[key] });
            if (_.isUndefined(obj)) {
                result.push({key: key, value: value[key]});
            }
        }
        return result;
    }
}