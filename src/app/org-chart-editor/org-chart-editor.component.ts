import { Component, ElementRef, Renderer, OnInit } from '@angular/core';


import { Label } from './entities/label';
import { Line } from "./entities/line";
import { Person } from "./entities/person";
import { DialogService } from './services/dialog.service';
import { LabelService } from "./services/label.service";
import { LineService } from "./services/line.service";
import { PersonService } from './services/person.service';
import {Subject} from "rxjs/Subject";


declare var _: any;

@Component({
    selector: 'org-chart-editor',
    templateUrl: './org-chart-editor.component.html'
})
export class OrgChartEditorComponent implements OnInit {

    width   : number;
    height  : number;
    labels  : Label[];
    lines   : Line[];
    people  : Person[];

    setEditablePersonSubject: Subject<any>  = new Subject();
    setEditableLabelSubject: Subject<any>   = new Subject();

    constructor(
        private el: ElementRef,
        private renderer: Renderer,
        private dialogService: DialogService,
        private labelService: LabelService,
        private lineService: LineService,
        private personService: PersonService
    ) {
        renderer.setElementStyle(el.nativeElement, 'display', '-webkit-box');
        renderer.setElementStyle(el.nativeElement, 'width', '1000px');
        renderer.setElementStyle(el.nativeElement, 'height', '700px');
    }

    ngOnInit() {
        this.getLabels();
        this.getLines();
        this.getPeople();
    }

    getLabels(): void {
        this.labelService.getLabels()
            .subscribe(labels => this.labels = labels);
    }

    addLabel(label: Label): void {
        this.labelService.addLabel(label)
            .subscribe(label => {
                this.labels.push(label);
            });
    }

    editLabel(params: any): void {
        this.setEditableLabelSubject.next(params.labelId);
        this.openDialog(params.dialogId);
    }

    updateLabel(label: Label): void {
        let index = _.findIndex(this.labels, function(labelItem) { return labelItem.id == label.id });
        this.labels[index] = label;

        this.labelService.updateLabel(label).subscribe();
    }

    deleteLabel(id: number): void {
        this.labels = this.labels.filter(label => label.id !== id);
        this.labelService.deleteLabel(id).subscribe();
    }

    getLines(): void {
        this.lineService.getLines()
            .subscribe(lines => this.lines = lines);
    }

    addLine(line: Line): void {
        this.lineService.addLine(line)
            .subscribe(line => {
                this.lines.push(line);
            });
    }

    getPeople(): void {
        this.personService.getPeople()
            .subscribe(people => this.people = people);
    }

    addPerson(person: Person): void {
        this.personService.addPerson(person)
            .subscribe(person => {
                this.people.push(person);
            });
    }

    editPerson(params: any): void {
        this.setEditablePersonSubject.next(params.personId);
        this.openDialog(params.dialogId);
    }

    updatePerson(person: Person): void {
        let index = _.findIndex(this.people, function(personItem) { return personItem.id == person.id });
        this.people[index] = person;

        this.personService.updatePerson(person).subscribe();
    }

    deletePerson(id: number): void {
        this.people = this.people.filter(person => person.id !== id);
        this.personService.deletePerson(id).subscribe();
    }

    openDialog(id: string) {
        this.dialogService.open(id);
    }

    closeDialog(id: string) {
        this.dialogService.close(id);
    }

// •	Constructor(jqueryObj, getData, saveData, getPeopleList, readonly)
//     o	jqueryObject is a div into which the org chart editor will be created
//     o	getData is a function that will return a data structure containing the intitial data to draw
//     o	saveData is a function that takes the same structure as above and saves it.
//     o	getPeopleList(search: string, external: bool) is a function that returns a list of people to potentially add to the page. The two parameters are passed from the GUI.
//     o	Readonly makes the chart read only and not editable.
// •	Delete
//     This causes the org chart to be deleted from the div and no longer be functional. It must be recreated.
}