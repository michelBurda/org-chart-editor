import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';


import { Person } from "../entities/person";


const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PersonService {

    private peopleUrl = 'api/people';

    constructor(
        private http: HttpClient
    ) { }

    getPeople(): Observable<Person[]> {
        return this.http.get<Person[]>(this.peopleUrl)
            .pipe(
                tap(people => console.log(`fetched people`)),
                catchError(this.handleError('getPerson', []))
            );
    }

    getPerson(id: number): Observable<Person> {
        const url = `${this.peopleUrl}/${id}`;
        return this.http.get<Person>(url).pipe(
            tap(_ => console.log(`fetched person id=${id}`)),
            catchError(this.handleError<Person>(`getPerson id=${id}`))
        );
    }

    addPerson (person: Person): Observable<Person> {
        return this.http.post<Person>(this.peopleUrl, person, httpOptions).pipe(
            tap((person: Person) => console.log(`added person w/ id=${person.id}`)),
            catchError(this.handleError<Person>('addPerson'))
        );
    }

    deletePerson (person: Person | number): Observable<Person> {
        const id = typeof person === 'number' ? person : person.id;
        const url = `${this.peopleUrl}/${id}`;

        return this.http.delete<Person>(url, httpOptions).pipe(
            tap(_ => console.log(`deleted person id=${id}`)),
            catchError(this.handleError<Person>('deletePerson'))
        );
    }

    updatePerson (person: Person): Observable<any> {
        return this.http.put(this.peopleUrl, person, httpOptions).pipe(
            tap(_ => console.log(`updated person id=${person.id}`)),
            catchError(this.handleError<any>('updatePerson'))
        );
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error:any):Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }
}