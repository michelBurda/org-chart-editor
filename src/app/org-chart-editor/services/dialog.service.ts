declare var _: any;

export class DialogService {
    private dialogs: any[] = [];

    add(dialog: any) {
        // add dialog to array of active dialogs
        this.dialogs.push(dialog);
    }

    remove(id: string) {
        // remove dialog from array of active dialogs
        let dialogToRemove = _.findWhere(this.dialogs, { id: id });
        this.dialogs = _.without(this.dialogs, dialogToRemove);
    }

    open(id: string) {
        // open dialog specified by id
        let dialog = _.findWhere(this.dialogs, { id: id });
        dialog.open();
    }

    close(id: string) {
        // close dialog specified by id
        let dialog = _.find(this.dialogs, { id: id });
        dialog.close();
    }
}