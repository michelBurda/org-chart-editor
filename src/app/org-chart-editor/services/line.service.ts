import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';


import { Line } from "../entities/line";


const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class LineService {

    private linesUrl = 'api/lines';

    constructor(
        private http: HttpClient
    ) { }

    getLines(): Observable<Line[]> {
        return this.http.get<Line[]>(this.linesUrl)
            .pipe(
                tap(lines => console.log(`fetched lines`)),
                catchError(this.handleError('getLines', []))
            );
    }

    getLine(id: number): Observable<Line> {
        const url = `${this.linesUrl}/${id}`;
        return this.http.get<Line>(url).pipe(
            tap(_ => console.log(`fetched line id=${id}`)),
            catchError(this.handleError<Line>(`getLine id=${id}`))
        );
    }

    addLine (line: Line): Observable<Line> {
        return this.http.post<Line>(this.linesUrl, line, httpOptions).pipe(
            tap((line: Line) => console.log(`added line w/ id=${line.id}`)),
            catchError(this.handleError<Line>('addLine'))
        );
    }

    deleteLine (line: Line | number): Observable<Line> {
        const id = typeof line === 'number' ? line : line.id;
        const url = `${this.linesUrl}/${id}`;

        return this.http.delete<Line>(url, httpOptions).pipe(
            tap(_ => console.log(`deleted line id=${id}`)),
            catchError(this.handleError<Line>('deleteLine'))
        );
    }

    updateLine (line: Line): Observable<any> {
        return this.http.put(this.linesUrl, line, httpOptions).pipe(
            tap(_ => console.log(`updated line id=${line.id}`)),
            catchError(this.handleError<any>('updateLine'))
        );
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error:any):Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }
}