import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';

import { Label } from "../entities/label";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class LabelService {

    private labelsUrl = 'api/labels';

    constructor(
        private http: HttpClient
    ) { }

    getLabels(): Observable<Label[]> {
        return this.http.get<Label[]>(this.labelsUrl)
            .pipe(
                tap(labels => console.log(`fetched labels`)),
                catchError(this.handleError('getLabels', []))
            );
    }

    getLabel(id: number): Observable<Label> {
        const url = `${this.labelsUrl}/${id}`;
        return this.http.get<Label>(url).pipe(
            tap(_ => console.log(`fetched label id=${id}`)),
            catchError(this.handleError<Label>(`getLabel id=${id}`))
        );
    }

    addLabel (label: Label): Observable<Label> {
        return this.http.post<Label>(this.labelsUrl, label, httpOptions).pipe(
            tap((label: Label) => console.log(`added label w/ id=${label.id}`)),
            catchError(this.handleError<Label>('addLabel'))
        );
    }

    deleteLabel (label: Label | number): Observable<Label> {
        const id = typeof label === 'number' ? label : label.id;
        const url = `${this.labelsUrl}/${id}`;

        return this.http.delete<Label>(url, httpOptions).pipe(
            tap(_ => console.log(`deleted label id=${id}`)),
            catchError(this.handleError<Label>('deleteLabel'))
        );
    }

    updateLabel (label: Label): Observable<any> {
        return this.http.put(this.labelsUrl, label, httpOptions).pipe(
            tap(_ => console.log(`updated label id=${label.id}`)),
            catchError(this.handleError<any>('updateLabel'))
        );
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error:any):Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }
}