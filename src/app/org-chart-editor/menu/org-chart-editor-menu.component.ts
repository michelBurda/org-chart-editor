import { Component, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'org-chart-editor-menu',
    templateUrl: './org-chart-editor-menu.component.html'
})
export class OrgChartEditorMenuComponent {

    @Output() addPersonEvent    = new EventEmitter();
    @Output() addLineEvent      = new EventEmitter();
    @Output() addLabelEvent     = new EventEmitter();

    constructor() {}

    addPerson() {
        this.addPersonEvent.next();
    }

    addLine() {
        this.addLineEvent.next();
    }

    addLabel() {
        this.addLabelEvent.next();
    }
}