export enum LineStyle {
    VH      = 0,
    HV      = 1,
    VHV     = 2,
    HVH     = 3,
    Free    = 4,
    VHD     = 256,
    HVD     = 257,
    VHVD    = 258,
    HVHD    = 259,
    FreeD   = 260
}