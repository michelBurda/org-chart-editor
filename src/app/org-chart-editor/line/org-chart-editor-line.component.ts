import { Component, Input } from '@angular/core';
import { Line } from "../entities/line";

declare var jQuery: any;

@Component({
    selector: 'org-chart-editor-line',
    templateUrl: './org-chart-editor-line.component.html'
})
export class OrgChartEditorLineComponent {
    @Input() line: Line;

    constructor() {}
}