import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';


import { Person } from '../entities/person';
import {Subject} from "rxjs/Subject";
import {PersonService} from "../services/person.service";


@Component({
    selector: 'org-chart-editor-person-create',
    templateUrl: './org-chart-editor-person-create.component.html',
    encapsulation: ViewEncapsulation.None
})
export class OrgChartEditorPersonCreateComponent implements OnInit {

    id: number;

    @Input() dialogId: string;
    @Input() setEditablePersonSubject: Subject<any>;

    @Output() addPersonEvent        = new EventEmitter();
    @Output() updatePersonEvent     = new EventEmitter();
    @Output() closeDialogEvent      = new EventEmitter();

    personCreateForm: FormGroup;

    constructor(private personService: PersonService) {}

    ngOnInit() {
        this.personCreateForm = new FormGroup({
            x: new FormControl(),
            y: new FormControl(),
            name: new FormControl(),
            position: new FormControl(),
            image: new FormControl()
        });

        this.setEditablePersonSubject.subscribe(param => {
            this.setEditable(param);
        });
    }

    setEditable(id: number) {
        this.id = id;

        this.personService.getPerson(this.id).subscribe(
            person => {
                this.personCreateForm.patchValue({
                    x:          person.x,
                    y:          person.y,
                    name:       person.name,
                    position:   person.position,
                    image:      person.image
                });
            }, error => {
                console.log(error);
            }
        );
    }

    onSubmit() {
        if (this.personCreateForm.valid) {

            if (this.id) {
                let person: Person = new Person(this.id,
                    this.personCreateForm.controls['x'].value,
                    this.personCreateForm.controls['y'].value,
                    this.personCreateForm.controls['name'].value,
                    this.personCreateForm.controls['position'].value,
                    this.personCreateForm.controls['image'].value);

                this.updatePersonEvent.next(person);
            } else {
                let person:Person = new Person(null,
                    this.personCreateForm.controls['x'].value,
                    this.personCreateForm.controls['y'].value,
                    this.personCreateForm.controls['name'].value,
                    this.personCreateForm.controls['position'].value,
                    this.personCreateForm.controls['image'].value
                );

                this.addPersonEvent.next(person);
            }

            if (this.dialogId) {
                this.closeDialogEvent.next(this.dialogId);
            }
        }
        this.personCreateForm.reset();
    }

}