import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { TextStyle } from '../enums/text-style.enum';

import { Label } from '../entities/label';
import {LabelService} from "../services/label.service";
import {Subject} from "rxjs/Subject";


@Component({
    selector: 'org-chart-editor-label-create',
    templateUrl: './org-chart-editor-label-create.component.html',
    encapsulation: ViewEncapsulation.None
})
export class OrgChartEditorLabelCreateComponent implements OnInit {

    id: number;

    @Input() dialogId: string;
    @Input() setEditableLabelSubject: Subject<any>;

    @Output() addLabelEvent     = new EventEmitter();
    @Output() updateLabelEvent  = new EventEmitter();
    @Output() closeDialogEvent  = new EventEmitter();

    labelCreateForm: FormGroup;

    textStyles = TextStyle;

    constructor(private labelService: LabelService) {}

    ngOnInit() {
        this.labelCreateForm = new FormGroup({
            text: new FormControl(),
            x: new FormControl(),
            y: new FormControl(),
            size: new FormControl(),
            style: new FormControl()
        });

        this.setEditableLabelSubject.subscribe(param => {
            this.setEditable(param);
        });
    }

    setEditable(id: number) {
        this.id = id;

        this.labelService.getLabel(this.id).subscribe(
            label => {
                this.labelCreateForm.patchValue({
                    text:   label.text,
                    x:      label.x,
                    y:      label.y,
                    size:   label.size,
                    style:  label.style
                });
            }, error => {
                console.log(error);
            }
        );
    }

    onSubmit() {
        if (this.labelCreateForm.valid) {

            if (this.id) {
                let label: Label = new Label(this.id,
                    this.labelCreateForm.controls['text'].value,
                    this.labelCreateForm.controls['x'].value,
                    this.labelCreateForm.controls['y'].value,
                    this.labelCreateForm.controls['size'].value,
                    this.labelCreateForm.controls['style'].value);
                
                this.updateLabelEvent.next(label);
            } else {
                let label:Label = new Label(null,
                    this.labelCreateForm.controls['text'].value,
                    this.labelCreateForm.controls['x'].value,
                    this.labelCreateForm.controls['y'].value,
                    this.labelCreateForm.controls['size'].value,
                    this.labelCreateForm.controls['style'].value
                );

                this.addLabelEvent.next(label);
            }

            if (this.dialogId) {
                this.closeDialogEvent.next(this.dialogId);
            }
        }

        this.labelCreateForm.reset();
    }

}