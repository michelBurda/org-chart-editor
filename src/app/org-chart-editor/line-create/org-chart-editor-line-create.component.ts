import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { EntranceType } from '../enums/entrance-type.enum';
import { LineStyle } from '../enums/line-style.enum';

import { Line } from '../entities/line';


@Component({
    selector: 'org-chart-editor-line-create',
    templateUrl: './org-chart-editor-line-create.component.html',
    encapsulation: ViewEncapsulation.None
})
export class OrgChartEditorLineCreateComponent implements OnInit {

    @Input() dialogId: string;
    @Output() addLineEvent      = new EventEmitter();
    @Output() closeDialogEvent  = new EventEmitter();

    lineCreateForm: FormGroup;

    entranceTypes = EntranceType;
    lineStyles = LineStyle;

    constructor() {}

    ngOnInit() {
        this.lineCreateForm = new FormGroup({
            fromId: new FormControl(),
            toId: new FormControl(),
            fromPos: new FormControl(),
            toPos: new FormControl(),
            style: new FormControl(),
            lineColor: new FormControl()
        });
    }

    onSubmit() {
        if (this.lineCreateForm.valid) {

            let line: Line = new Line(null,
                this.lineCreateForm.controls['fromId'].value,
                this.lineCreateForm.controls['toId'].value,
                this.lineCreateForm.controls['fromPos'].value,
                this.lineCreateForm.controls['toPos'].value,
                this.lineCreateForm.controls['style'].value,
                this.lineCreateForm.controls['lineColor'].value
            );

            this.addLineEvent.next(line);
            
            if (this.dialogId) {
                this.closeDialogEvent.next(this.dialogId);
            }
        }
        this.lineCreateForm.reset();
    }

}