import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Label } from "../entities/label";


@Component({
    selector: 'org-chart-editor-label',
    templateUrl: './org-chart-editor-label.component.html'
})
export class OrgChartEditorLabelComponent {
    @Input() label: Label;
    @Input() dialogId: string;

    @Output() editLabelEvent    = new EventEmitter();
    @Output() deleteLabelEvent  = new EventEmitter();

    editLabel() {
        this.editLabelEvent.next({dialogId: this.dialogId, labelId: this.label.id});
    }

    deleteLabel() {
        let result = confirm("Do you really want to delete this item?");
        if (result) {
            this.deleteLabelEvent.next(this.label.id);
        }
    }
}