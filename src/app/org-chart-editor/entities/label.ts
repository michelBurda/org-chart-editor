import { TextStyle } from '../enums/text-style.enum';


export class Label {
    id      : number;
    text    : string;
    x       : number;
    y       : number;
    size    : number;
    style   : TextStyle;

    constructor(id: number, text: string, x: number, y: number, size: number, style: TextStyle) {
        this.id     = id;
        this.text   = text;
        this.x      = x;
        this.y      = y;
        this.size   = size;
        this.style  = style;
    }
}