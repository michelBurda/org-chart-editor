import { LineStyle } from '../enums/line-style.enum';
import { EntranceType } from '../enums/entrance-type.enum';

export class Line {
    id          : number;
    fromId      : number;
    toId        : number;
    fromPos     : EntranceType;
    toPos       : EntranceType;
    style       : LineStyle;
    lineColor   : string;

    constructor(
        id          : number,
        fromId      : number,
        toId        : number,
        fromPos     : EntranceType,
        toPos       : EntranceType,
        style       : LineStyle,
        lineColor   : string
    ) {
        this.id         = id;
        this.fromId     = fromId;
        this.toId       = toId;
        this.fromPos    = fromPos;
        this.toPos      = toPos;
        this.style      = style;
        this.lineColor  = lineColor;
    }
}