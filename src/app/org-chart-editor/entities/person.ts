export class Person {
    id          : number;
    x           : number;
    y           : number;
    name        : string;
    position    : string;
    image       : string;

    constructor(
        id          : number,
        x           : number,
        y           : number,
        name        : string,
        position    : string,
        image       : string
    ) {
        this.id         = id;
        this.x          = x;
        this.y          = y;
        this.name       = name;
        this.position   = position;
        this.image      = image;
    }
}