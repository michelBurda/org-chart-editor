import { Person } from "../entities/person";


export const PEOPLE: Person[] = [
    new Person(1, 60, 100, "John Smith", "CEO", "img_avatar2.png"),
    new Person(2, 500, 100, "Mary Williams", "CTO", "img_avatar2.png"),
    new Person(3, 60, 300, "Rich Andrews", "Director of Marketing", "img_avatar2.png"),
    new Person(4, 500, 300, "Kathy Thompson", "Programmer", "img_avatar2.png")
];