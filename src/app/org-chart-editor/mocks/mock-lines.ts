import { Line } from "../entities/line";


export const LINES: Line[] = [
    new Line(1, 1, 2, 1, 2, 1, 'black'),
    new Line(2, 1, 3, 1, 2, 1, 'black'),
    new Line(3, 1, 4, 1, 2, 1, 'black'),
    new Line(4, 2, 4, 1, 2, 1, 'black')
];