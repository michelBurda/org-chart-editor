import { InMemoryDbService } from 'angular-in-memory-web-api';


import { LABELS } from './mocks/mock-labels';
import { LINES } from './mocks/mock-lines';
import { PEOPLE } from './mocks/mock-people';


export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        return {
            labels: LABELS,
            lines: LINES,
            people: PEOPLE
        };
    }
}