import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Person } from "../entities/person";


@Component({
    selector: 'org-chart-editor-person',
    templateUrl: './org-chart-editor-person.component.html'
})
export class OrgChartEditorPersonComponent {
    @Input() person: Person;
    @Input() dialogId: string;

    @Output() editPersonEvent   = new EventEmitter();
    @Output() deletePersonEvent = new EventEmitter();


    editPerson() {
        this.editPersonEvent.next({dialogId: this.dialogId, personId: this.person.id});
    }

    deletePerson() {
        let result = confirm("Do you really want to delete this item?");
        if (result) {
            this.deletePersonEvent.next(this.person.id);
        }
    }
}