import { ElementRef, NgZone, Directive } from '@angular/core';

declare var jQuery: any;

@Directive({
    selector: '[selectable]',
})
export class SelectableDirective {

    $selectable: any;

    constructor(private el: ElementRef, private zone: NgZone) {
        this.zone.runOutsideAngular(() => {
            this.$selectable = jQuery(this.el.nativeElement).selectable();
        });
    }
}
