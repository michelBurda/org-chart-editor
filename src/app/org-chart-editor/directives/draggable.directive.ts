import { ElementRef, NgZone, Directive } from '@angular/core';

declare var jQuery: any;

@Directive({
    selector: '[draggable]',
})
export class DraggableDirective {

    $draggable: any;

    constructor(private el: ElementRef, private zone: NgZone) {
        this.zone.runOutsideAngular(() => {
            this.$draggable = jQuery(this.el.nativeElement).draggable();
        });
    }
}
