import { Component, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';


import { DialogService } from '../services/dialog.service';
declare var jQuery: any;


@Component({
    selector: '[dialog]',
    template: '<ng-content></ng-content>'
})

export class DialogComponent implements OnInit, OnDestroy {
    @Input() id: string;
    private element: any;

    constructor(private dialogService: DialogService, private el: ElementRef) {
        this.element = jQuery(el.nativeElement);

        this.element.dialog({
            autoOpen: false,
            modal: true
        });
    }

    ngOnInit(): void {
        let dialog = this;

        // ensure id attribute exists
        if (!this.id) {
            console.error('dialog must have an id');
            return;
        }

        // add self (this dialog instance) to the dialog service so it's accessible from controllers
        this.dialogService.add(this);
    }

    // remove self from modal service when directive is destroyed
    ngOnDestroy(): void {
        this.dialogService.remove(this.id);
        this.element.remove();
    }

    // open dialog
    open(): void {
        this.element.dialog('open');
    }

    // close dialog
    close(): void {
        this.element.dialog('close');
    }
}