import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
    ReactiveFormsModule,
    FormsModule,
    FormGroup,
    FormControl,
    Validators,
    FormBuilder
} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './org-chart-editor/in-memory-data.service';


import { AppComponent } from './app.component';
import { DraggableDirective } from './org-chart-editor/directives/draggable.directive';
import { SelectableDirective } from './org-chart-editor/directives/selectable.directive';
import { DialogComponent } from './org-chart-editor/directives/dialog.component';
import { OrgChartEditorComponent } from './org-chart-editor/org-chart-editor.component';
import { OrgChartEditorMenuComponent } from './org-chart-editor/menu/org-chart-editor-menu.component';
import { OrgChartEditorPersonComponent } from './org-chart-editor/person/org-chart-editor-person.component';
import { OrgChartEditorLabelComponent } from './org-chart-editor/label/org-chart-editor-label.component';
import { OrgChartEditorLineComponent } from './org-chart-editor/line/org-chart-editor-line.component';
import { OrgChartEditorLabelCreateComponent } from './org-chart-editor/label-create/org-chart-editor-label-create.component';
import { OrgChartEditorPersonCreateComponent } from './org-chart-editor/person-create/org-chart-editor-person-create.component';
import { OrgChartEditorLineCreateComponent } from './org-chart-editor/line-create/org-chart-editor-line-create.component';
import { ToKeyValuePairPipe } from './org-chart-editor/pipes/to-key-value-pair.pipe';
import { DialogService } from './org-chart-editor/services/dialog.service';
import { LabelService } from './org-chart-editor/services/label.service';
import { LineService } from './org-chart-editor/services/line.service';
import { PersonService } from './org-chart-editor/services/person.service';


@NgModule({
  declarations: [
    AppComponent,
    DraggableDirective,
    SelectableDirective,
    OrgChartEditorComponent,
    OrgChartEditorMenuComponent,
    OrgChartEditorPersonComponent,
    OrgChartEditorLabelComponent,
    OrgChartEditorLineComponent,
    ToKeyValuePairPipe,
    DialogComponent,
    OrgChartEditorLabelCreateComponent,
    OrgChartEditorPersonCreateComponent,
    OrgChartEditorLineCreateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
        InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [
    DialogService,
    LabelService,
    LineService,
    PersonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
